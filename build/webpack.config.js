module.exports = {
    output: {
        filename: 'main.js',
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/env']
                }
            },
        ],

    },
    externals: {
    },
    mode: 'development'
};
