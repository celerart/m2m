$(document).ready(function () {
    //open menu
    $('.open-menu').click(function () {
        //$('body').addClass('menu-open');
        $('.header-right').addClass('menu-open');
    });
    $('.close-menu').click(function () {
        //$('body').removeClass('menu-open');
        $('.header-right').removeClass('menu-open');
    });

    $('body').on('click', '.open-popup', function (e) {
        e.preventDefault();
        $('.modal').fadeOut(300);
        var modal = $(this).data('target');
        $(modal).fadeIn(300);
        $('body').addClass('modal-open');
    });

    $('body').on('click', '.reset-password-send', function () {
        $('.modal').fadeOut(300);
        var modal = $(this).data('target');
        $(modal).fadeIn(300);
    });

    //scrollbar
    $('.scroll-content').mCustomScrollbar();
    $('.scroll-content-x').mCustomScrollbar({axis: 'x'});

    //close modal
    $('.modal').on('click', function (e) {
        var div = $('.modal-content');
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            $('.modal').fadeOut(300);
            $('body').removeClass('modal-open');
        }
    });
    $(document).keyup(function (e) {
        if (e.key === 'Escape') {
            $('.modal').fadeOut(300);
            $('body').removeClass('modal-open');
        }
    });
    $('.close-modal').on('click', function (e) {
        $('.modal').fadeOut(300);
        $('body').removeClass('modal-open');
    });
    $('.close-popup').on('click', function (e) {
        $('.modal').fadeOut(300);
        $('body').removeClass('modal-open');
    });


    //show lang dropdown
    $('.dropdown-current').click(function () {

        //$('.option-item.dropdown-wrap').removeClass('open');

        if ($(this).parent().hasClass('open')) {
            $(this).parent().removeClass('open');
        } else {
            $('.option-item.dropdown-wrap').removeClass('open');
            $(this).parent().addClass('open');
        }
        if ($(window).width() < 767) {
            //$('.main-header').css('display', 'none');
            //$('body').toggleClass('menu-open');
            if ($(this).hasClass('header-lang_current')) {
                $(this).parent().find('.header-lang_dropdown').slideToggle(300);
            }
            if ($(this).hasClass('option-item_default')) {
                $('.main-header').css('display', 'none');
                //$('body').toggleClass('menu-open');
                $('body').addClass('fixed');
            }
            if ($(this).hasClass('open-location-dropdown')) {
                $('.location-panel_top').fadeIn(300);
                $('body').addClass('fixed');
            }
        }
    });

    //hide lang dropdown
    $('body').on('click', function (e) {
        var div = $(".dropdown-wrap");
        var daterangePicker = $('.daterangepicker');
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            if (e.target.closest('th') || e.target.closest('.daterangepicker')) {

            } else {
                //$('body').removeClass('menu-open');
                $('.dropdown-wrap').removeClass('open');
                $('.main-header').css('display', 'block');
            }
        }
        //console.dir(e.target.closest('.daterangepicker'));
    });

    //hide mobile search item
    $('.close-dropdown').click(function () {
        $('.main-header').css('display', 'block');
        //$('body').removeClass('menu-open');
        $('.location-panel_top').fadeOut(300);
        $('body').removeClass('fixed');
        $(this).parents('.dropdown-wrap').removeClass('open');
    });
    $('.apply-filters').click(function () {
        $('.main-header').css('display', 'block');
        $('body').removeClass('fixed');
        $(this).parents('.dropdown-wrap').removeClass('open');
        $(this).parents('.filters-panel_top').fadeOut(300);
        $(this).parents('.location-panel_top').fadeOut(300);
    });

    //fixed header
    var lastScrollTop = 0, lastScrollTopFloat = 0, lastScrollTopNav = 0, lastScrollTopSearch = 0, delta = 5;
    $(window).scroll(function () {
        fixedHeader();
        floatBlock();
        pagePropertyStickyNav();
        carrersFloatBlock();
        searchHeaderMobile();
        hideMobileBookNow();
    });

    fixedHeader();

    function getDocumentHeight() {
        return Math.max(
            Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
            Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
            Math.max(document.body.clientHeight, document.documentElement.clientHeight)
        );
    }

    function hideMobileBookNow() {
        if ($('.float-wrapper .float-block').length > 0) {
            if ($(window).width() < 991) {
                var docHeight = getDocumentHeight();
                if ($(window).scrollTop() + window.innerHeight + 70 > docHeight) {
                    $('.float-wrapper .float-block').addClass('bottom');
                } else {
                    $('.float-wrapper .float-block').removeClass('bottom');
                }
            }
        }
    }

    function searchHeaderMobile() {
        if ($(window).width() < 767) {
            //console.log($('#search-page'));
            if (pageYOffset > 0) {
                var nowScrollTopSearch = $(this).scrollTop();
                if (Math.abs(lastScrollTopSearch - nowScrollTopSearch) >= delta) {
                    if (nowScrollTopSearch > lastScrollTopSearch) {
                        $('.search-panel_top').addClass('scroll-down');
                    } else {
                        $('.search-panel_top').removeClass('scroll-down');
                    }
                    lastScrollTopSearch = nowScrollTopSearch;
                }
            }
        }
    }

    function fixedHeader() {
        if (!$('body').hasClass('property-page')) {
            if (0 == pageYOffset) {
                setTimeout(function () {
                    $('.main-header').removeClass('scrolled');
                    $('.main-header').removeClass('scroll-down');
                }, 300)
            }
            var nowScrollTop = $(this).scrollTop();
            if (Math.abs(lastScrollTop - nowScrollTop) >= delta) {
                if (nowScrollTop > lastScrollTop) {
                    $('.main-header').removeClass('scrolled');
                    $('.main-header').addClass('scroll-down');
                } else {
                    $('.main-header').addClass('scrolled');
                    $('.main-header').removeClass('scroll-down');
                }
                lastScrollTop = nowScrollTop;
            }
        } else {
            if ($(window).width() < 1200) {
                var nowScrollTop = $(this).scrollTop();
                if (Math.abs(lastScrollTop - nowScrollTop) >= delta) {
                    if (nowScrollTop > lastScrollTop) {
                        $('.main-header').removeClass('scrolled');
                        $('.main-header').addClass('scroll-down');
                    } else {
                        $('.main-header').addClass('scrolled');
                        $('.main-header').removeClass('scroll-down');
                    }
                    lastScrollTop = nowScrollTop;
                }
            }
            if (0 == pageYOffset) {
                setTimeout(function () {
                    $('.main-header').removeClass('scrolled');
                    $('.main-header').removeClass('scroll-down');
                }, 300)
            }
        }
        if ($(window).scrollTop() < 200) {
            $('.main-header').removeClass('scroll-down');
        }
    }

    //property page float block
    function floatBlock() {
        if ($('.property-tabs_content').length > 0) {
            var propertyHeight = $('.property-info').offset().top;
            var propertyAmenities = $('.tab-content_item.amenities').offset().top;
            var propertyCancelation = $('.tab-content_item.cancelation').offset().top;
            if (pageYOffset > propertyCancelation) {
                $('.property-tabs_header').addClass('show-third-button');
            } else {
                $('.property-tabs_header').removeClass('show-third-button');
            }
            if (pageYOffset > propertyHeight) {
                $('.property-tabs_content .float-block').addClass('fixed');
                //detect direction of the scrolling
                var nowScrollTop = $(this).scrollTop();
                if (Math.abs(lastScrollTopFloat - nowScrollTop) >= delta) {
                    if (nowScrollTop < lastScrollTopFloat) {
                        //scroll top
                        $('.property-tabs_content .float-block').addClass('move-down');
                    } else {
                        //scroll bottom
                        $('.property-tabs_content .float-block').removeClass('move-down');
                    }
                    lastScrollTopFloat = nowScrollTop;
                }
            } else {
                $('.property-tabs_content .float-block').removeClass('fixed move-down');
            }
        }
    }


    //careers page float block
    var lastScrollTopFloatCareers = 0;

    function carrersFloatBlock() {
        if ($('.sticky-inner').length > 0) {
            //detect direction of the scrolling
            var nowScrollTopCareers = $(this).scrollTop();
            if (Math.abs(lastScrollTopFloatCareers - nowScrollTopCareers) >= delta) {
                if (nowScrollTopCareers < lastScrollTopFloatCareers) {
                    //scroll top
                    $('.sticky-inner').addClass('move-down');
                } else {
                    //scroll bottom
                    $('.sticky-inner').removeClass('move-down');
                }
                lastScrollTopFloatCareers = nowScrollTopCareers;
            }
        }
    }

    //property page fixed navigation
    function pagePropertyStickyNav() {
        if ($('.property-tabs_header').length > 0) {
            //detect croll to the block
            var propertyHeight = $('.section-properties').offset().top;
            if (pageYOffset > propertyHeight) {
                $('.property-tabs_header').addClass('scroll-down');
                //detect direction of the scrolling
                var nowScrollTop = $(this).scrollTop();
                if (Math.abs(lastScrollTopNav - nowScrollTop) >= delta) {
                    if (nowScrollTop < lastScrollTopNav) {
                        //scroll top
                        $('.property-tabs_header').addClass('fixed');
                    } else {
                        //scroll bottom
                        $('.property-tabs_header').removeClass('fixed');
                    }
                    lastScrollTopNav = nowScrollTop;
                }
            } else {
                $('.property-tabs_header').removeClass('scroll-down');
                $('.property-tabs_header').removeClass('fixed');
            }
        }
    }

    //search options
    $('.switcher-item').click(function () {
        var tab = $(this).attr('data-tab');
        $(this).parents('.filters').find('.switcher-item').removeClass('active');
        $(this).parents('.filters').find('.switcher-tab_item').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.filters').find('.switcher-tab_item.' + tab).addClass('active');
    });

    //available terms
    $('.available-item').click(function () {
        $('.available-item').removeClass('active');
        $(this).addClass('active');
    });

    //show items
    $('.search-item_head').click(function (e) {
        var div = $(".dropdown-wrap");
        var buttons = $('.view-buttons');
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            if (!buttons.is(e.target) && buttons.has(e.target).length === 0) {
                $(this).parents('.search-item').toggleClass('open');
                //$(this).parents('.search-item').find('.search-item_body').fadeToggle(300);
            }
        }
    });

    //switch view
    $('.view-item').click(function () {
        var view = $(this).attr('data-view');
        $(this).parent().find('.view-item').removeClass('active')
        $(this).addClass('active');
        $(this).parents('.search-item').find('.result-items').removeClass('grid list');
        $(this).parents('.search-item').find('.result-items').addClass(view);
    });

    //scroll top
    $('.to-top').click(function () {
        $('html,body').animate({scrollTop: $('body').offset().top}, 1000);
    });

    //select region proposals
    $('body').on('click', '.search-head .dropdown-item', function () {
        $(this).parents('.search-head').addClass('active');
        $('.search-head .dropdown-item').removeClass('current');
        $(this).addClass('current')
        var region = $(this).find('.title').html();
        var proposal = $(this).find('.proposal').html();
        $('.region-selected').html(region);
        $('.search-head_proposal').html(proposal);
        $('.dropdown-wrap').removeClass('open');
    });

    //search dropdown options
    $('.dropdown-item.selectable').click(function () {
        $(this).toggleClass('selected')
    });

    //quantity change
    $('.quantity-input .minus').click(function () {
        var input = $(this).parent().find('input.qty');
        var inputDefault = $(this).parent().find('.value');
        var curValue = $(this).parent().find('.value').html();
        var count = parseInt(input.val()) - 1;
        count = count < 0 ? 0 : count;
        if (0 == count) {
            $(this).addClass('disabled');
        }
        input.val(count);
        inputDefault.html(count);
        input.change();

        //number of guests desktop
        if ($(this).parents('.item-dropdown').find('input[name=guests]').length > 0) {
            var adultsValue = $(this).parents('.item-dropdown').find('input[name=adults]').val();
            var childrenValue = $(this).parents('.item-dropdown').find('input[name=children]').val();
            var str = adultsValue + ' Adults, ' + childrenValue + ' Children';
            $(this).parents('.item-dropdown').find('input[name=guests]').val(str);

            //toggle book total
            if ($('input[name=stay-for]').val() || $('input[name=check-in]').val()) {
                $('.order-total_info').slideDown(300)
            }
        }
        //stay for months desktop
        else if ($(this).parents('.item-dropdown').find('input[name=stay-for]').length > 0) {
            var stayFor = $(this).parents('.item-dropdown').find('input[name=months]').val();
            var strStay = stayFor + ' Months';
            $(this).parents('.item-dropdown').find('input[name=stay-for]').val(strStay);
        }
        //stay for months mobile
        else if ($(this).parents('.filter-panel_elem').hasClass('stay-for-mobile-main')) {
            var stayForMobile = $(this).parents('.filters-panel_top').find('input[name=months-mobile]').val();
            var strStayMobile = stayForMobile + ' Months';
            $(this).parents('.filters-panel_top').find('input[name=stay-for-mobile-main]').val(strStayMobile);
        }
        //number of guests mobile
        else if ($(this).parents('.filter-panel_elem').hasClass('guests-mobile-main')) {
            var adultsValueMobile = $(this).parents('.filters-panel_top').find('input[name=adults-mobile]').val();
            var childrenValueMobile = $(this).parents('.filters-panel_top').find('input[name=children-mobile]').val();
            var strMobile = adultsValueMobile + ' Adults, ' + childrenValueMobile + ' Children';
            $(this).parents('.filters-panel_top').find('input[name=guests-mobile-main]').val(strMobile);

            //toggle book total
            if ($('input[name=stay-for-mobile-main]').val() || $('input[name=check-in-mobile-main]').val()) {
                $('.order-total_info').slideDown(300)
            }
        }

        return false;
    });
    $('.quantity-input .plus').click(function () {
        $(this).parent().find('.minus').removeClass('disabled');
        var input = $(this).parent().find('input.qty');
        var inputDefault = $(this).parent().find('.value');
        var curValue = $(this).parent().find('.value').html();
        input.val(parseInt(input.val()) + 1);
        var count = input.val();
        input.val(count);
        inputDefault.html(count);
        input.change();

        //number of guests desktop
        if ($(this).parents('.item-dropdown').find('input[name=guests]').length > 0) {
            var adultsValue = $(this).parents('.item-dropdown').find('input[name=adults]').val();
            var childrenValue = $(this).parents('.item-dropdown').find('input[name=children]').val();
            var str = adultsValue + ' Adults, ' + childrenValue + ' Children';
            $(this).parents('.item-dropdown').find('input[name=guests]').val(str);

            //toggle book total
            if ($('input[name=stay-for]').val() || $('input[name=check-in]').val()) {
                $('.order-total_info').slideDown(300)
            }
        }
        //stay for months desktop
        else if ($(this).parents('.item-dropdown').find('input[name=stay-for]').length > 0) {
            var stayFor = $(this).parents('.item-dropdown').find('input[name=months]').val();
            var strStay = stayFor + ' Months';
            $(this).parents('.item-dropdown').find('input[name=stay-for]').val(strStay);
        }
        //stay for months mobile
        else if ($(this).parents('.filter-panel_elem').hasClass('stay-for-mobile-main')) {
            var stayForMobile = $(this).parents('.filters-panel_top').find('input[name=months-mobile]').val();
            var strStayMobile = stayForMobile + ' Months';
            $(this).parents('.filters-panel_top').find('input[name=stay-for-mobile-main]').val(strStayMobile);
        }
        //number of guests mobile
        else if ($(this).parents('.filter-panel_elem').hasClass('guests-mobile-main')) {
            var adultsValueMobile = $(this).parents('.filters-panel_top').find('input[name=adults-mobile]').val();
            var childrenValueMobile = $(this).parents('.filters-panel_top').find('input[name=children-mobile]').val();
            var strMobile = adultsValueMobile + ' Adults, ' + childrenValueMobile + ' Children';
            $(this).parents('.filters-panel_top').find('input[name=guests-mobile-main]').val(strMobile);

            //toggle book total
            if ($('input[name=stay-for-mobile-main]').val() || $('input[name=check-in-mobile-main]').val()) {
                $('.order-total_info').slideDown(300)
            }
        }


        return false;
    });

    //calendar init
    if ($('.dates .dropdown-current').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.dates .dropdown-current').daterangepicker({
            'minDate': moment().format('MM/DD/YYYY'),
            'parentEl': $('.dates-calendar-wrapper'),
            'locale': locale,
        });
        $('.dates .dropdown-current').on('apply.daterangepicker', function (ev, picker) {
            $('.checkout-dates').html(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            var startDay = new Date(picker.startDate);
            var endDay = new Date(picker.endDate);
            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / (1000 * 3600 * 24);
            var nights = Math.round(Math.abs(days)) - 1;
            $('.nights span').html(nights);

            $('.option-item.dropdown-wrap.dates').removeClass('open')
        });

        $('.dates .dropdown-current').on('cancel.daterangepicker', function (ev, picker) {
            $('.nights span').html(1);
            $('.checkout-dates').html('12.17.2020 - 12.17.2020');

            $('.option-item.dropdown-wrap.dates').removeClass('open');
        });
    }
    //search calendars
    if ($('.search-calendar_dates').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.search-calendar_dates').daterangepicker({
            'locale': locale,
            'minDate': moment().format('MM/DD/YYYY'),
            'parentEl': $('#move-in-out-mobile'),
        });
        $('.search-calendar_dates').on('apply.daterangepicker', function (ev, picker) {
            $('.checkout-dates').html(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            var startDay = new Date(picker.startDate);
            var endDay = new Date(picker.endDate);
            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / (1000 * 3600 * 24);
            var nights = Math.round(Math.abs(days)) - 1;
            $('.nights span').html(nights);
            //close move-in-section
            $('.filter-panel_elem.move-in-out').removeClass('open');
            $('.filter-panel_inner').removeClass('side-open');
        });

        $('.search-calendar_dates').on('cancel.daterangepicker', function (ev, picker) {
            $('.nights span').html(1);
            $('.checkout-dates').html('12.17.2020 - 12.17.2020');
            //close move-in-section
            $('.filter-panel_elem.move-in-out').removeClass('open');
            $('.filter-panel_inner').removeClass('side-open');
        });
    }
    if ($('.get-date').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.get-date').daterangepicker({
            'locale': locale,
            'minDate': moment().format('MM/DD/YYYY'),
            singleDatePicker: true,
            isInvalidDate: function (ele) {
                var currDate = moment(ele._d).format('YY-MM-DD');
                return ['21-01-16', '21-01-23'].indexOf(currDate) != -1;
            }
        });
    }


    //animation
    new WOW().init();

    //sliders
    //dropdown items slider
    if ($('.search-item_slider').length > 0) {
        $('.search-item_slider').slick({
            arrows: false,
            dots: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: false,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        arrows: true,
                        slidesToShow: 1,
                        infinite: true,
                        centerMode: true,
                        centerPadding: '16px'
                    }
                }
            ]
        });

        $(".search-item_slider").on("afterChange", function (slick, currentSlide) {
            var curSlide = currentSlide.currentSlide + 1;
            if (slick.target.classList.contains('search-item_slider')) {
                $(this).parents('.search-item').find('.count-slider .current').html(curSlide);
            }
        });
    }

    //dropdown items image slider
    if ($('.image-slider').length > 0) {
        $('.image-slider').slick({
            arrows: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: false,
            infinite: true,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        arrows: false,
                        swipe: true
                    }
                }
            ]
        });
        // setTimeout(function(){
        //     $('.home .search-item_body').css('display', 'none');
        //     /*if ($(window).width() > 767) {
        //         $('.search-page .search-item_body').css('display', 'none');
        //     }*/
        // }, 1000);

        if ($('.sp-content_result .image-slider').parents('.result-items').find('.count-slider').length > 0) {
            $(".sp-content_result .image-slider").on("afterChange", function (slick, currentSlide) {
                var curSlide = currentSlide.currentSlide + 1;
                $(this).parents('.house-item').find('.count-slider .current').html(curSlide);
            });
        }
    }

    //special offers script
    if ($('.special-offer_items.desktop').length > 0) {
        $('.special-offer_items.desktop').slick({
            arrows: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            cssEase: 'ease-out',
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
        });
        $('.special-offer_items.mobile').slick({
            arrows: true,
            dots: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        centerMode: true,
                        centerPadding: '20px',
                        slidesToShow: 1
                    }
                }
            ]

        });

        $(".special-offer_items.mobile").on("afterChange", function (slick, currentSlide) {
            var curSlide = currentSlide.currentSlide + 1;
            $('.special-offers-section .count-slider .current').html(curSlide);
        });
    }

    //reviews slider
    if ($('.reviews-items').length > 0) {
        $('.reviews-items').slick({
            arrows: true,
            dots: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '80px',
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        centerPadding: '70px',
                    }
                },
                {
                    breakpoint: 1300,
                    settings: {
                        centerPadding: '40px',
                    }
                },
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 2,
                        centerPadding: '40px',
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        centerPadding: '20px',
                    }
                }
            ]
        });


        $(".reviews-items").on("beforeChange", function (slick, currentSlide) {
            var curSlide = currentSlide.currentSlide;
            $(".reviews-image img").fadeOut(300);
            $(".reviews-image img.image-" + curSlide).fadeIn(300);
        });
        $(".reviews-items").on("afterChange", function (slick, currentSlide) {
            var curSlide = currentSlide.currentSlide + 1;
            $('.reviews .count-slider .current').html(curSlide);
        });
    }

    //property page gallery mobile
    $('.section-gallery_mobile .property-gallery_slider').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        prevArrow: '<button class="slick-prev"></button>',
        nextArrow: '<button class="slick-next"></button>'
    });

    $('select').niceSelect();

    /******************Search page***************/

    //switch view on mobile
    /*function changeViewOnMobile() {
        if ($(window).width() < 768) {
            $('.search-page .sp-content_result .search-items .result-items').removeClass('list');
        } else {
            $('.search-page .sp-content_result .search-items .result-items:not(.list)').addClass('list');
        }
    }

    changeViewOnMobile();

    $(window).resize(function(){
        changeViewOnMobile();
    });*/

    //add to favorite
    $('.add-to-favorite').click(function () {
        $(this).toggleClass('added');
    });

    //search page form dropdowns
    $('body').on('click', '.sf-dropdown .item-dropdown', function () {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
        } else {
            $('.sf-dropdown .item-dropdown').removeClass('open');
            $(this).addClass('open');
        }
        $('.sf-dropdown .item-dropdown:not(.open) .default-state').fadeIn(300);
        if (!$(this).parent().hasClass('dates')) {
            $(this).find('.default-state').fadeOut(300);
        }
        //$(this).find('.input').focus();
    });

    $('body').on('click', function (e) {
        var div = $(".search-form");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            $('.default-state').fadeIn(300);
            $('.item-dropdown').removeClass('open');
        }
    });

    //profile links
    $('.panel-item_button').click(function () {
        var block = $(this).attr('data-target');
        $('.panel-item_button').removeClass('active');
        $(this).addClass('active');
        $('.search-page_block').fadeOut(300);
        $('.search-page_block.sp-' + block).fadeIn(300);
    });

    //toggle dropdown
    $('body').on('click', '.toggle-dropdown .filter-item_name.toggle', function () {
        $(this).toggleClass('open');
        $(this).parent().find('.filter_item_dropdown').slideToggle(300);
    });
    //toggle sideSlide
    $('body').on('click', '.toggle-dropdown .filter-item_name.toggle-side', function () {
        $('.search-calendar_dates').trigger('click');
        $(this).parents('.filter-panel_inner').addClass('side-open');
        $('.filter-panel_elem.move-in-out').addClass('open');
    });
    // $('body').on('click', '.drp-buttons .btn', function(){
    //     $('.filter-panel_inner').removeClass('side-open');
    // });
    $('body').on('click', '.go-back', function () {
        $('.filter-panel_inner').removeClass('side-open');
        $('.filter-panel_elem').removeClass('open');
    });

    //open search filters
    $('body').on('click', '.open-search-filters', function () {
        $('.filters-panel_top').fadeIn(300);
        setTimeout(function () {
            $('body').addClass('fixed');
        }, 300);

    });
    $('body').on('click', '.close-filter', function () {
        $('body').removeClass('fixed');
        $('.filters-panel_top').fadeOut(300);
        $(this).removeClass('menu-open');
    });

    //search checkboxes
    $('.dropdown-options input[type=checkbox]').change(function () {
        var number = 0;
        $(this).parents('.checkboxes').find('input[type=checkbox]').each(function () {
            if ($(this).prop('checked') == true) {
                number++;
            }
        });
        if (0 == number) {
            $(this).parents('.dropdown-options').find('.apply-filter_item').fadeOut(300);
        } else {
            $(this).parents('.dropdown-options').find('.apply-filter_item').fadeIn(300);
        }
    });

    /********************Property page***************/
    //play video
    //stop video
    $('.video-hover').click(function (e) {
        var button = $(".play-video");
        var video = $(this).parents('.video-block_inner').find('iframe');
        var viseoSrc;
        if (!button.is(e.target) && button.has(e.target).length === 0) {
            $(this).removeClass('invisible');
            var defautlSrc = video.attr('src').split('?')
            viseoSrc = defautlSrc[0] + '?controls=0';
        } else {
            $(this).addClass('invisible');
            viseoSrc = video.attr('src') + '&autoplay=1';
        }
        video.attr('src', viseoSrc);
    });

    //open gallery
    $('.view-all-photos').click(function () {
        $(this).parents('.property-gallery').find('.image-item:first-child a').trigger('click');
    });

    //scroll tabs
    $('.tab-button').click(function () {
        var tabTarget = $(this).attr('data-target');
        $(this).parents('.tabs-wrapper').find('.nav-tab_item').removeClass('active');
        $(this).parent().addClass('active');
        if ($('.tab-content_item.' + tabTarget).length > 0) {
            $('html,body').animate({scrollTop: $('.tab-content_item.' + tabTarget).offset().top}, 1000);
        }
    });

    //book now check-in
    if ($('.book-check-in').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.book-check-in').daterangepicker({
            "autoApply": true,
            'minDate': moment().format('MM/DD/YYYY'),
            autoUpdateInput: false,
            'parentEl': $('#checkin-dates'),
            singleDatePicker: true,
            'locale': locale,
        });

        $('.book-check-in').on('apply.daterangepicker', function (ev, picker) {
            $('input[name=check-in]').val(picker.startDate.format('MM.DD.YYYY'));
        });

        // $('.book-check-in').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        // });
    }
    //book now check-in mobile
    if ($('.book-check-in-mobile').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.book-check-in-mobile').daterangepicker({
            "autoApply": true,
            'minDate': moment().format('MM/DD/YYYY'),
            autoUpdateInput: false,
            'parentEl': $('#checkin-dates-mobile'),
            singleDatePicker: true,
            'locale': locale,
        });

        $('.book-check-in-mobile').on('apply.daterangepicker', function (ev, picker) {
            $('input[name=check-in-mobile-main]').val(picker.startDate.format('MM.DD.YYYY'));
            //$('.filter-panel_inner').removeClass('side-open');
            //$('.filter-panel_elem.check-in-mobile-main').removeClass('open');
        });

        // $('.book-check-in').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        // });
    }
    //book now stay-for
    if ($('.book-stay-for').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.book-stay-for').daterangepicker({
            "autoApply": true,
            'minDate': moment().format('MM/DD/YYYY'),
            autoUpdateInput: false,
            'parentEl': $('#stay-for'),
            singleDatePicker: true,
            'locale': locale,
        });

        $('.book-stay-for').on('apply.daterangepicker', function (ev, picker) {
            $('input[name=stay-for]').val(picker.startDate.format('MM.DD.YYYY'));
        });

        // $('.book-stay-for').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        // });
    }
    //book now stay-for mobile
    if ($('.book-stay-for-mobile').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.book-stay-for-mobile').daterangepicker({
            "autoApply": true,
            'minDate': moment().format('MM/DD/YYYY'),
            autoUpdateInput: false,
            'parentEl': $('#stay-for-mobile'),
            singleDatePicker: true,
            'locale': locale,
        });

        $('.book-stay-for-mobile').on('apply.daterangepicker', function (ev, picker) {
            $('input[name=stay-for-mobile-main]').val(picker.startDate.format('MM.DD.YYYY'));
            //$('.filter-panel_inner').removeClass('side-open');
            //$('.filter-panel_elem.check-in-mobile-main').removeClass('open');
        });

        // $('.book-check-in').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        // });
    }

    //book now calendar
    if ($('.inline-calendar').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.inline-calendar').daterangepicker({
            autoUpdateInput: false,
            'minDate': moment().format('MM/DD/YYYY'),
            'parentEl': $('#book-now_inline'),
            'locale': locale,
        });

        $('.inline-calendar').trigger('click');

        $('.inline-calendar').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MM.DD.YYYY') + ' - ' + picker.endDate.format('MM.DD.YYYY'));
        });

        $('.inline-calendar').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

    }
    //mobile calendar
    if ($('.inline-calendar_mobile').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };
        $('.inline-calendar_mobile').daterangepicker({
            autoUpdateInput: false,
            'minDate': moment().format('MM/DD/YYYY'),
            singleDatePicker: true,
            'parentEl': $('.tab-content_mobile'),
            'locale': locale,
        });

        $('.inline-calendar_mobile').trigger('click');

        $('.inline-calendar_mobile').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MM.DD.YYYY') + ' - ' + picker.endDate.format('MM.DD.YYYY'));
        });

        $('.inline-calendar_mobile').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

    }

    //view more
    $('.view-more').click(function () {
        $(this).parent().fadeOut(300);
        $(this).parent().parent().find('.view-more_wrap').addClass('open');
    });

    //choose on calendar
    $('.choose-on-calendar').click(function () {
        var calendarTarget = $(this).attr('data-target');
        $('.book-now.' + calendarTarget).trigger('click');
    });

    //show book total mobile
    $('.show-book-summary').click(function (e) {
        e.preventDefault();
        if ($(window).width() < 991) {
            $('.filters-panel_top').fadeIn(300);
            setTimeout(function () {
                $('body').addClass('fixed');
            }, 300);
        }
    });

    //hide form on check-in change
    $('input[name=check-in-period]').change(function () {
        $('input[name=check-in-period]').each(function () {
            if ($(this).prop('checked') == true) {
                var checkInList = $(this).val();
                $(this).parents('.item-dropdown').find('input[name=check-in]').val(checkInList);
                $(this).parents('.item-dropdown').removeClass('open');
            }
        })
    });

    //property page booking mobile
    $('.pi-wrap_form label').click(function () {
        var inputName = $(this).find('input').attr('name');
        $(this).parents('.filter-panel_inner').addClass('side-open');
        $('.filter-panel_elem.' + inputName).addClass('open');
    });

    //hide mobile form on check-in change
    $('input[name=check-in-period-mobile]').change(function () {
        $('input[name=check-in-period-mobile]').each(function () {
            if ($(this).prop('checked') == true) {
                var checkInList = $(this).val();
                $(this).parents('.filters-panel_top').find('input[name=check-in-mobile-main]').val(checkInList);
                //$(this).parents('.filter-panel_elem').removeClass('open');
                //$(this).parents('.filters-panel_top').find('.filter-panel_inner').removeClass('side-open');
            }
        })
    });

    //apply filters mobile form
    $('body').on('click', '.apply-filters-mobile', function () {
        $('.filter-panel_elem').removeClass('open');
        $('.filters-panel_top .filter-panel_inner').removeClass('side-open');
    });

    /**************************Home owner page*********************/
    //text accordion
    $('.text-item_heading').click(function () {
        if ($(window).width() < 768) {
            $(this).toggleClass('open');
            $(this).parents('.text-item').find('.text-item_content').slideToggle(300);
        }
    });

    //read more
    var maxLength = 100;
    $('.pr-item_text p').each(function () {
        var myStr = $(this).text();
        if ($.trim(myStr).length > maxLength) {
            var oldStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
            $(this).empty().html(oldStr);
            $(this).append('<button class="read-more">... <span>Read more.</span></button>');
            $(this).append('<span class="more-text">' + removedStr + '</span>');
        }
    });
    $('body').on('click', '.read-more', function () {
        $(this).siblings('.more-text').contents().unwrap();
        $(this).remove();
    });

    //property reviews slider
    if ($('.pr-slider').length > 0) {
        $('.pr-slider').slick({
            arrows: true,
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        arrows: false,
                        dots: false,
                        centerMode: true,
                        centerPadding: '20px',
                        adaptiveHeight: true
                    }
                }
            ]
        });


        $(".pr-slider").on("afterChange", function (slick, currentSlide) {
            var curSlide = currentSlide.currentSlide + 1;
            $('.reviews .count-slider .current').html(curSlide);
        });
    }

    /**************************FAQ page*********************/
    //site tabs
    $('body').on('click', '.stn-item_button', function () {
        if ($(this).data('tab') == 'sign-up') {
            $(this).parents('.site-tabs_nav').addClass('sign-up');
        } else {
            $(this).parents('.site-tabs_nav').removeClass('sign-up');
        }
        if ($(this).parents('ul').hasClass('slick-initialized')) {
            var slideno = $(this).parents('.slick-slide').data('slick-index');
            $('.site-tabs_nav ul').slick('slickGoTo', slideno);
        }
        var tabTarget = $(this).attr('data-tab');
        $(this).parents('.site-tabs').find('.stn-item_button').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.site-tabs').find('.st-content_item').removeClass('active');
        $('.st-content_item.' + tabTarget).addClass('active');
    });

    //tabs mobile slider
    $(window).resize(function () {
        initTabsSliderMobile();
    });

    initTabsSliderMobile();

    function initTabsSliderMobile() {
        if ($(window).width() < 767) {
            setTimeout(function () {
                $('.site-tabs_nav.slider-tabs-mobile ul').not('.slick-initialized').slick({
                    arrows: false,
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    centerMode: true,
                    centerPadding: '70px'
                });
            }, 500)
        } else {
            $('.site-tabs_nav.slider-tabs-mobile ul.slick-initialized').slick('unslick');
        }
    }

    /**************************Careers page*********************/
    //open accordion
    $('.accordion-button').click(function () {
        // $('.site-accordion_item .sa-item_body').slideUp(300);
        // $('.site-accordion_item .accordion-button').removeClass('open');
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).parents('.site-accordion_item').find('.sa-item_body').slideUp(300);
        } else {
            $('.site-accordion_item .sa-item_body').slideUp(300);
            $('.site-accordion_item .accordion-button').removeClass('open');
            $(this).addClass('open');
            $(this).parents('.site-accordion_item').find('.sa-item_body').slideDown(300);
        }
    });

    //add file
    $('.sticky-inner input[type=file]').change(function () {
        if ($(this).val()) {
            $(this).parents('.file-item').find('.input-file').fadeOut(300);
            $(this).parents('.file-item').find('.file-added').fadeIn(300);
        }
    });
    //remove file
    $('.file-added').click(function () {
        $(this).parents('.file-item').find('input[type=file]').val('');
        $(this).parents('.file-item').find('.input-file').fadeIn(300);
        $(this).fadeOut(300);
    });

    /**************************Payments page*********************/
    //change payment button text
    function paymentButtonText() {
        $('input[name=payment-method]').each(function () {
            if ($(this).prop('checked') == true) {
                var paymentName = $(this).val();
                $('.payment-methods .button .payment-method_name').html(paymentName);
            }
        });
    }

    $('input[name=payment-method]').change(function () {
        paymentButtonText();
    });

    paymentButtonText();

    //calculator
    //next step button
    $('.cc-item_button').click(function () {
        var i = 0;
        var prev = $(this).data('prev');
        var next = $(this).data('next');

        $('.sticky-form .radio-buttons input[type=radio]').each(function () {
            if ($(this).prop('checked') == true) {
                radioValue = $(this).val();
            }
        });
        // if ('Renter occupied' == radioValue) {
        //     $('#renter-mi').addClass('input-text');
        //     $('#renter-mo').addClass('input-text');
        //     $('#owner-mo').removeClass('input-text');
        //     $('select[name=rent-terms]').removeClass('select-item');
        // } else if ('Owner occupied' == radioValue) {
        //     $('#renter-mi').removeClass('input-text');
        //     $('#renter-mo').removeClass('input-text');
        //     $('#owner-mo').addClass('input-text');
        //     $('select[name=rent-terms]').removeClass('select-item');
        // } else if ('Vacant' == radioValue) {
        //     $('#renter-mi').removeClass('input-text');
        //     $('#renter-mo').removeClass('input-text');
        //     $('#owner-mo').removeClass('input-text');
        //     $('select[name=rent-terms]').addClass('select-item');
        // }

        $(this).parents('.sticky-form').find('.form-item input.input-text').each(function () {
            var value = $(this).val();
            var name = $(this).attr('name');
            if (value == '') {
                $(this).parents('.label').addClass('error');
                i++;
            } else {
                $(this).parents('.label').removeClass('error');
            }
            $(this).parents('.calculator-content').find('.summary-item.' + name + ' .name').html(value);
        });

        $(this).parents('.sticky-form').find('.form-item select.select-item').each(function () {
            var value = $(this).val();
            var name = $(this).attr('name');
            if (value == 'Select option') {
                $(this).parents('.label').addClass('error');
                i++;
            } else {
                $(this).parents('.label').removeClass('error');
            }
            $(this).parents('.calculator-content').find('.summary-item.' + name + ' .name').html(value);
        });

        $('.sticky-form .radio-buttons input[type=radio]').each(function(){
            var name = $(this).attr('name');
            $(this).parents('.radio-buttons').find('input[type=radio]').each(function () {
                if ($(this).prop('checked') == true) {
                    radioValue = $(this).val();
                }
            });
            $(this).parents('.calculator-content').find('.summary-item.' + name + ' .name').html(radioValue);
        });

        if (0 == i) {
            $('.cs-item.item-' + prev).removeClass('active');
            $('.cs-item.item-' + next).addClass('active');
            $('.cs-item.item-' + next).addClass('passed');
            $('.cs-item.item-' + prev).addClass('passed');
            $('.calculator-content .cc-item').css('display', 'none');
            $('.calculator-content .cc-item.item-' + next).css('display', 'block');

            $('html,body').animate({scrollTop: $('#calculator-content').offset().top-100}, 1000);
        }
    });

    //step navigation
    $('body').on('click', '.cs-item.passed .cs-item_button', function () {
        var step = $(this).data('step');
        var stepNext = parseInt(step)+1;
        $('.cs-item').removeClass('active');
        //$('.cs-item.item-'+stepNext).removeClass('passed');
        $(this).parents('.cs-item').addClass('active');
        $('.calculator-content .cc-item').css('display', 'none');
        $('.calculator-content .cc-item.item-' + step).css('display', 'block');
    });

    //prev step
    $('body').on('click', '.cc-item_prev', function () {
        var step = $(this).data('step');
        var stepNext = parseInt(step)+1;
        $('.cs-item').removeClass('active');
        $('.cs-item.item-'+step).addClass('active');
        //$('.cs-item.item-'+stepNext).removeClass('passed');
        $('.calculator-content .cc-item').css('display', 'none');
        $('.calculator-content .cc-item.item-' + step).css('display', 'block');

        if ($(window).width() < 767) {
            $('html,body').animate({scrollTop: $('#calculator-content').offset().top-100}, 1000);
        }
    });



    //radio buttons checked
    var radioValue = '';
    $('.sticky-form .radio-buttons input[type=radio]').change(function () {
        var name = $(this).attr('name');
        $(this).parents('.radio-buttons').find('input[type=radio]').each(function () {
            if ($(this).prop('checked') == true) {
                radioValue = $(this).val();
            }
        });
        $(this).parents('.calculator-content').find('.summary-item.' + name + ' .name').html(radioValue);

        if (!$(this).parents('.radio-buttons').hasClass('simple')) {
            var tab = $(this).data('tab');

            $(this).parents('.sticky-form').find('.occupancy-content_item').css('display', 'none');
            $(this).parents('.sticky-form').find('.occupancy-content_item.' + tab).css('display', 'block');
            $(this).parents('.sticky-form').find('.buttons .cc-item_button').css('display', 'block');

            if ('Renter occupied' == radioValue) {
                $('#renter-mi').addClass('input-text');
                $('#renter-mo').addClass('input-text');
                $('#renter-rate').addClass('input-text');
                $('#owner-mo').removeClass('input-text');
                $('select[name=rent-terms]').removeClass('select-item');

                $('.summary .summary-item').css('display', 'block');

                $('.summary .summary-item.rented-before').css('display', 'none');
                $('.summary .summary-item.rent-available').css('display', 'none');
                $('.summary .summary-item.rent-terms').css('display', 'none');
            } else if ('Owner occupied' == radioValue) {
                $('#renter-mi').removeClass('input-text');
                $('#renter-mo').removeClass('input-text');
                $('#renter-rate').removeClass('input-text');
                $('#owner-mo').addClass('input-text');
                $('select[name=rent-terms]').removeClass('select-item');

                $('.summary .summary-item.move-out').css('display', 'block');
                $('.summary .summary-item.move-in').css('display', 'none');
                $('.summary .summary-item.current-rate').css('display', 'none');

                $('.summary .summary-item.rented-before').css('display', 'none');
                $('.summary .summary-item.rent-available').css('display', 'none');
                $('.summary .summary-item.rent-terms').css('display', 'none');
            } else if ('Entity or trust' == radioValue) {
                $('#another-first-name').removeClass('input-text');
                $('#another-last-name').removeClass('input-text');
                $('#another-email').removeClass('input-text');
                $('#another-phone').removeClass('input-text');

                $('#entity-name').addClass('input-text');
            } else if ('Another individual' == radioValue) {
                $('#another-first-name').addClass('input-text');
                $('#another-last-name').addClass('input-text');
                $('#another-email').addClass('input-text');
                $('#another-phone').addClass('input-text');

                $('#entity-name').removeClass('input-text');
            } else if ('Me' == radioValue) {
                $('#another-first-name').removeClass('input-text');
                $('#another-last-name').removeClass('input-text');
                $('#another-email').removeClass('input-text');
                $('#another-phone').removeClass('input-text');

                $('#entity-name').removeClass('input-text');
            } else {
                $('#renter-mi').removeClass('input-text');
                $('#renter-mo').removeClass('input-text');
                $('#renter-rate').removeClass('input-text');
                $('#owner-mo').removeClass('input-text');
                $('select[name=rent-terms]').addClass('select-item');

                $('.summary .summary-item.move-out').css('display', 'none');
                $('.summary .summary-item.move-in').css('display', 'none');
                $('.summary .summary-item.current-rate').css('display', 'none');

                $('.summary .summary-item.rented-before').css('display', 'block');
                $('.summary .summary-item.rent-available').css('display', 'block');
                $('.summary .summary-item.rent-terms').css('display', 'block');
            }
        }

        if ($(window).width() < 767) {
            $('html,body').animate({scrollTop: $('#occupancy-content').offset().top-50}, 1000);
        }

    });

    //calculator datepicker
    if ($('input.select-date').length > 0) {
        var locale = {
            firstDay: 1,
            daysOfWeek: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        };

        if ($(window).width() < 767) {
            $('input.select-date').daterangepicker({
                'locale': locale,
                'minDate': moment().format('MM/DD/YYYY'),
                singleDatePicker: true,
                autoUpdateInput: false,
                'parentEl': $('.filters-panel_top .filter-elem_body'),
            });
        } else {
            $('input.select-date').daterangepicker({
                'locale': locale,
                'minDate': moment().format('MM/DD/YYYY'),
                singleDatePicker: true,
                autoUpdateInput: false,
            });
        }
        $('input.select-date').on('apply.daterangepicker', function (ev, picker) {
            if ($(this).attr('name') == 'move-out') {
                $('input[name=move-out]').val(picker.startDate.format('MM/DD/YYYY'));
            }
            $(this).val(picker.startDate.format('MM/DD/YYYY'));
            $(this).parents('.label').removeClass('error');

            if ($(window).width() < 767) {
                $('.filters-panel_top').fadeOut(300);
            }
        });

        $('input.select-date').on('cancel.daterangepicker', function (ev, picker) {
            if ($(window).width() < 767) {
                $('.filters-panel_top').fadeOut(300);
            }
        });

        $('input.select-date').on('focus', function(){
            if ($(window).width() < 767) {
                var title = $(this).parents('.form-item').find('.date-label > span').html();
                $('.filters-panel_top .filters-panel_head .filter-title').html(title);
                $('.filters-panel_top').fadeIn(300);
            } else {
                $('html,body').animate({scrollTop: $(this).offset().top - 250}, 1000);
            }
        })
    }
    //close mobile datepicker
    $('.clear-calculator-dates').click(function(){
        $(this).parents('.filters-panel_top').fadeOut(300);
    });

    if ($('#tts-date').length > 0) {
        $('#tts-date').daterangepicker({
            'locale': locale,
            'minDate': moment().format('MM/DD/YYYY'),
            singleDatePicker: true,
            autoUpdateInput: false,
            'parentEl': $('.modal .select-date_wrapper'),
        });

        $('#tts-date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY'));
            $(this).parents('.label').removeClass('error');
        });
    }

    //home owner slider
    if ($('.steps-section .step-items.mobile').length > 0) {
        $('.steps-section .step-items.mobile').slick({
            arrows: true,
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
        });
    }


    $(".steps-section .step-items.mobile").on("afterChange", function (slick, currentSlide) {
        var curSlide = currentSlide.currentSlide + 1;
        $(this).parents('.section').find('.count-slider .current').html(curSlide);
    });

    //contract correct name yes/no
    $('input[name=c-real-name]').change(function(){
        var realName = $('.contract-user-name').html();
        if ($(this).prop('checked') == true) {
            if ('No' == $(this).val()) {
                $('.form-item.user-name').css('display', 'block');
                $('input[name=c-user-name]').val('');
            } else {
                $('.form-item.user-name').css('display', 'none');
                $('input[name=c-user-name]').val(realName);
            }
        }
    });
        //open edit form
    $('.edit-profile').on('click', function(e){
        let el = $(e.target).parents('.form-row')
        el.addClass('active');
    });

    //remove edit form
    $('.save-profile').on('click', function(e){
        let el = $(e.target).parents('.form-row')
        el.removeClass('active');
    });
    //add active to from group
    $('.mobile-row').on('click', function(e){
        let el = $(e.target).parents('.col-title')
        $(e.target).parents('.mobile-row').toggleClass('active');
        el.children('.mobile-slide').slideToggle()
    });
    //add active to from group on focus
    $('.form_control:not(.select-date)').on('focus', function(e){

        let el = $(e.target).parents('.form_group')
        el.toggleClass('active');
    });
    //add active to from group on blur if empty val
    $('.form_control:not(.select-date)').on('blur', function(e){
        if($(e.target).val() == ''){
            let el = $(e.target).parents('.form_group')
            el.toggleClass('active');
        }
    });
    //add active to date input
    $('.form_control.select-date').on('apply.daterangepicker',function( e ){
        let elem = $(e.target).parents('.form_group')
        if($(e.target).val() == ''){
            elem.removeClass('active');
        } else {
            elem.addClass('active');
        }
    });

    //open dropdown
    $('.toogle-btn').on('click',function( e ){
        let elem = $(e.target).parents('.form_group')
        elem.children('.drop_list').slideToggle()
    });

    //set dropdown value
    $('.drop_list button').on('click',function( e ){
        let text = e.target.innerText;
        let elem = $(e.target).parents('.form_group')
        $(e.target).parents('.drop_list').slideToggle()
        elem.addClass('active');
        elem.children('.toogler').children('.toogle-btn').text(text)
    });

    // client testimonials
    $('.client-slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    // init thank popup
    $('.open-thank').on('click', function(){
        $('#thankPopup').toggleClass('open');
    });

    //open edit form
    $('.edit-pass').on('click', function(e){
        let el = $(e.target).parents('.pass-content')
        el.addClass('active');
    });

    //remove edit form
    $('.save-pass').on('click', function(e){
        let el = $(e.target).parents('.pass-content')
        el.removeClass('active');
    });


    //open mobile table on page reservation
    $('.mobile-open').on('click', function(e){
        let el = $(e.target).parents('.column-1')
        $(e.target).parents('.mobile-open').toggleClass('active');
        console.log(el);
        el.children('.mobile_col').children('.info_items').slideToggle()
    });

    // logged-in notification open
    $('.notification-btn').on('click', function(e){
        let el = $(e.target).parents('.notifi')
        el.children('.notifi-list').slideToggle()
    });

    // logged-in notification open
    $('.img_wrapper').on('click', function(e){
        let el = $(e.target).parents('.user_avatar')
        el.children('.user_menu').slideToggle()
    });
    //book_payment time dropdown
    $('.main_drop .drop-toogler').on('click', function (e){
        let el = $(e.target).parents('.main_drop')
        el.toggleClass('active');
        el.children('.drop_lists').slideToggle()

    });

    //book_payment time dropdown

    $('.main_drop .drop_lists button').on('click', function (e){
        let el = $(e.target).parents('.main_drop')
        let text = e.target.innerText;
        el.toggleClass('active');
        el.children('.drop-toogler').children('.toogle-btn').text(text);
        el.children('.drop_lists').slideToggle()

    });

    //new home_owner slider

    if ($('.home_owner_img-slider').length > 0) {
        $('.home_owner_img-slider').slick({
            arrows: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            infinite: false,
            centerPadding: '0',
            asNavFor: '.slider_thumbs',
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        centerPadding: '0',
                    }
                },
                {
                    breakpoint: 1300,
                    settings: {
                        centerPadding: '0',
                    }
                },
                {
                    breakpoint: 1199,
                    settings: {
                        centerPadding: '0',
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        centerPadding: '0',
                    }
                }
            ]
        });

        $('.slider_thumbs').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.home_owner_img-slider',
            dots: false,
            infinite: false,
            arrows: false,
            centerMode: true,
            focusOnSelect: true,
            centerPadding: '0',
            fade: true,
            cssEase: 'ease',
            speed: 100,
        });


        // $(".reviews-items").on("beforeChange", function (slick, currentSlide) {
        //     var curSlide = currentSlide.currentSlide;
        //     $(".reviews-image img").fadeOut(300);
        //     $(".reviews-image img.image-" + curSlide).fadeIn(300);
        // });
        // $(".reviews-items").on("afterChange", function (slick, currentSlide) {
        //     var curSlide = currentSlide.currentSlide + 1;
        //     $('.reviews .count-slider .current').html(curSlide);
        // });
    }




});