module.exports = {
    output: {
        filename: 'main.js',
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    "presets": [
                        [
                            "@babel/preset-env",
                            {
                                "useBuiltIns": "entry",
                                "targets": {
                                    "chrome": "58",
                                    "ie": "11"
                                }
                            }
                        ]
                    ]
                }
            }
        ]
    },
    externals: {
        jquery: 'jQuery',
    },
    mode: 'production'
};
